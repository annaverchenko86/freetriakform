DOCUMENTATION

  Not every CSS rule should be documented.
  You should document a rule declaration when the rule can accurately describe a visual UI element in the styleguide.

  Inline Sass Comments - document everything in order to reduce the potential problems.

  Write comments all in single-line Sass comments so that they do not get compiled into the generated CSS.

  Start every mixin, function and extend with a block of documentation explaining:

    - what the following piece of code does
    - what arguments it accepts (if any)
    - an example for how to use it

  // $width - String or number representation of the element width
  // $height - String or number representation of the element height
  // false if it is the same as the width
  //
  // Examples
  // @include size(10px);
  // @include size(10px, 30px);
  //

  @mixin size($width, $height: false) {
    width: $width;
    height: if($height, $height, $width);
  }

  Having documentation inline in our Sass makes it easy to see at a glance what things do as we’re working with our code.

  Must have doc blocks for classes, extends and anything else, explaining their meaning and pursope, to make it easy to see at a glance what things do.



  Each element should have one documentation block describing that particular UI element’s various states.

  // A button suitable for giving stars to someone.
  //
  // :hover             - Subtle hover highlight.
  // .stars-given       - A highlight indicating you've already given a star.
  // .stars-given:hover - Subtle hover highlight on top of stars-given styling.
  // .disabled          - Dims the button to indicate it cannot be used.
  //
  // Styleguide 2.1.3.
  a.button.star{
    ...
    &.star-given{
      ...
    }
    &.disabled{
      ...
    }
  }

  Each KSS documentation block consists of three parts:
    - a description of what the element does or looks like

        The description should be plain sentences of what the CSS rule or hierarchy does and looks like.
        A good description gives guidance toward the application of elements the CSS rules style.

        // A feed of activity items. Within each <section.feed>, there should be many
        // <article>s which are the  feed items.

    - a list of modifier classes or pseudo-classes and how they modify the element

        If the UI element you are documenting has multiple states or styles depending on added classes or pseudo-classes,
        you should document them in the modifiers section.

        // :hover             - Subtle hover highlight.
        // .stars-given       - A highlight indicating you've already given a star.
        // .stars-given:hover - Subtle hover highlight on top of stars-given styling.
        // .disabled          - Dims the button to indicate it cannot be used.


  To describe the status of a set of rules, you should prefix the description with "Experimental" or "Deprecated"

    Experimental  - indicates CSS rules that apply to experimental styling.
                    This can be useful when testing out new designs before they launch (staff only),
                    alternative layouts in A/B tests, or beta features.

                    // Experimental: An alternative signup button styling used in AB Test #195.


    Deprecated  - indicates that the rule is slated for removal.
                  Rules that are deprecated should not be used in future development.
                  This description should explain what developers should do when encountering this style.

                  // Deprecated: Styling for legacy wikis. We'll drop support for these wikis on
                  // July 13, 2007.




    - and a reference to the element’s position in the styleguide.



  Using SCSS or LESS, you should document all helper functions (sometimes called mixins):

    // Creates a linear gradient background, from top to bottom.
    //
    // $start - The color hex at the top.
    // $end   - The color hex at the bottom.
    //
    // Compatible in IE6+, Firefox 2+, Safari 4+.
    @mixin gradient($start, $end){
      ...
    }

  Each documentation block should have a description section, parameters section, and compatibility section.


  If the mixin takes parameters, you should document each parameter and describe what sort of input it expects (hex, number, etc).

    // $start - The color hex at the top.
    // $end   - The color hex at the bottom.

  The compatibility section - you must list out what browsers this helper method is compatible in.

    // Compatible in IE6+, Firefox 2+, Safari 4+.

  If you do not know the compatibility, you should state as such.

    // Compatibility untested.



  Styleguide

  In order to fully take advantage of KSS, you should create a living styleguide.

  The styleguide should be organized by numbered sections.
  These sections can go as deep as you like.
  Every element should have a numbered section to refer to.
  For example:

    1. Buttons
      1.1 Form Buttons
        1.1.1 Generic form button
        1.1.2 Special form button
      1.2 Social buttons
      1.3 Miscelaneous buttons
    2. Form elements
      2.1 Text fields
      2.2 Radio and checkboxes
    3. Text styling
    4. Tables
      4.1 Number tables
      4.2 Diagram tables

// For printing

KSS Documentation

Not every CSS rule should be documented. You should document a rule declaration when the rule can accurately describe a visual UI element in the styleguide.

Each KSS documentation block consists of three parts:
 - description of what the element does or looks like
 - list of modifier classes or pseudo-classes and how they modify the element
 - reference to the element’s position in the styleguide

1. The description section

    The description should be plain sentences of what the CSS rule or hierarchy does and looks like.

    To describe the status of a set of rules, you should prefix the description with "experimental" or "deprecated".

    Experimental - indicates CSS rules that apply to experimental styling.
                   This can be useful when testing out new designs before
                   they launch (staff only), alternative layouts in A/B tests,
                   or beta features.

        // Experimental: An alternative signup button styling used in AB Test #195.

    Deprecated - indicates that the rule is slated for removal.
                 Rules that are deprecated should not be used in future development.
                 This description should explain what developers should do when
                 encountering this style.

        // Deprecated: Styling for legacy wikis. We'll drop support for these wikis on
        // July 13, 2007.

2. The modifiers section

    If the UI element you are documenting has multiple states or styles depending on
    added classes or pseudo-classes, you should document them in the modifiers section.

        // :hover
        // .disabled
        // .some-class

3. The styleguide section

    If the UI element you are documenting has an example in the styleguide,
    you should reference it using the “Styleguide [ref]” syntax.

        // Styleguide 2.1.3.

    If there is no example, then you must note that there is no reference.

        // No styleguide reference.



Using SCSS or LESS, you should document all helper functions (sometimes called mixins):

    // Creates a linear gradient background, from top to bottom.
    //
    // $start - The color hex at the top.
    // $end   - The color hex at the bottom.
    //
    // Compatible in IE6+, Firefox 2+, Safari 4+.

    @mixin gradient($start, $end){
      ...
    }

1. The parameters section

    If the mixin takes parameters, you should document each parameter
    and describe what sort of input it expects (hex, number, etc).

    // $start - The color hex at the top.
    // $end   - The color hex at the bottom.

2. The compatibility section

    You must list out what browsers this helper method is compatible in.

        // Compatible in IE6+, Firefox 2+, Safari 4+.

    If you do not know the compatibility, you should state as such.

        // Compatibility untested.

3. The styleguide should be organized by numbered sections.
   These sections can go as deep as you like.
   Every element should have a numbered section to refer to. For example:

   1. Buttons
     1.1 Form Buttons
       1.1.1 Generic form button
       1.1.2 Special form button
     1.2 Social buttons
     1.3 Miscelaneous buttons
   2. Form elements
     2.1 Text fields
     2.2 Radio and checkboxes
   3. Text styling
   4. Tables
     4.1 Number tables
     4.2 Diagram tables